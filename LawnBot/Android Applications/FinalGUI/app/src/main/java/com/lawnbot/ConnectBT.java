package com.lawnbot;

//////////////////////////////////////////////////IMPORTS///////////////////////////////////////////
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

//////////////////////////////////////////////CONNECTBT/////////////////////////////////////////////
public class ConnectBT extends Application {
    //Initialize variables
    private BluetoothSocket mmSocket;
    private InputStream mmInStream;
    private OutputStream mmOutStream;
    private byte[] mmBuffer; // mmBuffer store for the stream
    private static ConnectBT sInstance;
    public String MACAddress = "B8:27:EB:E1:8F:3F";
    private UUID MY_UUID = UUID.fromString("936DA01F-9ABD-4D9D-80C7-02AF85C822A8");
    private static final String TAG = "MY_APP_DEBUG_TAG";
    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    BluetoothSocket btSocket = null;
    public Handler mHandler;

    /////////////////////////////////////ON CREATE//////////////////////////////////////////////////
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }//onCreate()

    public static ConnectBT getInstance() {
        return sInstance;
    }//getInstance()

    public void setupBluetoothConnection()
    {
        BluetoothDevice mDevice;
        BluetoothSocket tmp = null;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.isEmpty()) {
            return ;
        }
        for (BluetoothDevice device : pairedDevices) {
            if (MACAddress.equals(device.getAddress())) {
                    mDevice = device;
                try {
                    tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
                } catch (IOException e) {
                    Log.e(TAG, "Socket's create() method failed", e);
                }
                mmSocket = tmp;
                try {
                    mmSocket.connect();
                } catch (IOException connectException) {
                    // Unable to connect; close the socket and return.
                    try {
                        mmSocket.close();
                    } catch (IOException closeException) {
                        Log.e(TAG, "Could not close the client socket", closeException);
                    }
                    return;
                }

                //Setting up input and output streams for reading and writing
                    try {
                        tmpIn = mmSocket.getInputStream();
                    } catch (IOException e) {
                        Log.e(TAG, "Error occurred when creating input stream", e);
                    }
                    try {
                        tmpOut = mmSocket.getOutputStream();
                    } catch (IOException e) {
                        Log.e(TAG, "Error occurred when creating output stream", e);
                    }

                    mmInStream = tmpIn;
                    mmOutStream = tmpOut;
                    break;
                }
            }
        }

        public BluetoothSocket getCurrentBluetoothConnection()
        {
            return btSocket;
        }

        public InputStream getInputStream(){return mmInStream;}

        //writing to the pi
        public void write(String type, String value) {
            try {
                String output = (type + "," + value+"\n");//Sends Command and Parameter
                mmOutStream.write(output.getBytes());//Send as bytes
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when sending data", e);
            }
        }//write

    public String read() {
        mmBuffer = new byte[1024];
        int bytes;
        String message = null;
        try {
            // Read from the InputStream.
            bytes = mmInStream.read(mmBuffer);
            message = new String(mmBuffer, 0, bytes);
        } catch (IOException e) {
            Log.d(TAG, "Input stream was disconnected", e);
        }
        return message;
    }//read


    // Closing the socket
    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "Could not close the client socket", e);
        }
    }//cancel



    }//ConnectBT


