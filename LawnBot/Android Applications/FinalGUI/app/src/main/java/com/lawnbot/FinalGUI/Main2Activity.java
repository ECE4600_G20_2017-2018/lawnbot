package com.lawnbot.FinalGUI;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;

import com.lawnbot.ConnectBT;

public class Main2Activity extends MainActivity {

    public Button autonomousButton;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        autonomousButton = (Button) findViewById(R.id.autonomous);


        ConnectBT.getInstance().setupBluetoothConnection();
        ConnectBT.getInstance().getCurrentBluetoothConnection();

        //Two to One Button
        autonomousButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });//Two to One Button



    }
}
