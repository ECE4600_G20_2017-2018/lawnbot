package com.lawnbot.FinalGUI;

/////////////////////////////////////////////////IMPORTS////////////////////////////////////////////
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import java.lang.Math;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Editable;
import android.text.TextWatcher;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Spinner;
import android.widget.EditText;
import android.widget.Toast;

import com.lawnbot.ConnectBT;

////////////////////////////////////////////////MAIN////////////////////////////////////////////////
public class MainActivity extends Activity {
    private static final int REQUEST_ENABLE_BT = 1;
    //Initialize Layout Objects
    public Button stopButton;
    public Button goHomeButton;
    public Button connectButton;
    public Button startButton;
    public Button updateButton;
    public TextView batteryText;
    public TextView batteryLifeText;
    public EditText newMapName;
    //Lawn Selection Variables
    public int mapToCut;
    public String lawnSelected;
    public String lawnType;
    public List<String> lawnChoices;
    //Mapping/Drawing Variables
    public List<Float> X;
    public List<Float> Y;
    public List<Float> XMap;
    public List<Float> YMap;
    public Canvas mCanvas;
    public Bitmap mBitmap;
    public ImageView mapImageView;
    public Paint borderPaint;
    public Paint posPaint;
    public int borderColor = 0;
    public int clearColor = 0;
    public int posColor = 0;
    public View view;
    public int vWidth;
    public int vHeight;
    public int scaleFactor;
    public int ySpacing;
    public int xSpacing;
    public float xShift=0;
    public float yShift=0;
    //Bluetooth Variables
    public String messageReceived = null;
    public String messageCommand;
    public String messageArgument;
    public Runnable runnable;
    public Handler handler;



//////////////////////////////////////////////////ON CREATE/////////////////////////////////////////
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Define Layout
        setContentView(R.layout.activity_main);
        updateButton = findViewById(R.id.update);
        connectButton = findViewById(R.id.connectBT);
        stopButton = findViewById(R.id.stop);
        goHomeButton = findViewById(R.id.goHome);
        startButton = findViewById(R.id.start);
        batteryText = findViewById(R.id.batteryText);
        newMapName = findViewById(R.id.newMapText);
        mapImageView = findViewById(R.id.mapImageView);
        borderPaint = new Paint(Paint.UNDERLINE_TEXT_FLAG);
        posPaint = new Paint(Paint.UNDERLINE_TEXT_FLAG);
        batteryLifeText = findViewById(R.id.batteryLifeText);
        Spinner lawnSelection = findViewById(R.id.mapSelection);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lawnChoices);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Initialize thread for reading BT Inputstream
        new Thread(new BTListener()).start();

        //Ensure Bluetooth is turned on
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //Check if Bluetooth is supported
        if (mBluetoothAdapter == null) {
            //If BT is not supported on device
            Toast.makeText(getApplicationContext(),"Bluetooth Not Supported",Toast.LENGTH_SHORT).show();
        }
        //Request to enable Bluetooth
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        //Spinner Initialization
        lawnChoices =  new ArrayList<String>();
        lawnChoices.add("Select Lawn");
        lawnChoices.add("New Lawn");
        lawnSelection.setAdapter(adapter);

        //Setting up paint colors
        clearColor = ResourcesCompat.getColor(getResources(), R.color.colorGray2, null);
        posColor = ResourcesCompat.getColor(getResources(),R.color.colorPos,null);
        borderColor = ResourcesCompat.getColor(getResources(),R.color.colorBorder,null);
        borderPaint.setColor(borderColor);
        posPaint.setColor(posColor);
        X = new ArrayList<Float>();//received border x coordinates
        Y = new ArrayList<Float>();//received border y coordinates
        XMap = new ArrayList<Float>();//manipulated border x coordinates
        YMap = new ArrayList<Float>();//manipulated border y coordinates

///////////////////////////LAWN SELECTION LISTENERS/////////////////////////////////////////////////
        //Spinner Listener
        lawnSelection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                mapToCut = position;

                if (position ==0){//Select Lawn
                    newMapName.setEnabled(false);//Text box disabled
                    startButton.setEnabled(false);//Start button disabled
                    clearMap();
                }
                if (position == 1){//New Lawn
                    newMapName.setEnabled(true);//Tex box enabled
                    startButton.setEnabled(false);//Start button disabled
                    lawnType = "NewMap";
                    clearMap();
                }
                if (position>1){
                    newMapName.setEnabled(false);//Text box disabled
                    startButton.setEnabled(true);//Start button enabled
                    lawnSelected = lawnChoices.get(position);//Name of lawn selected
                    lawnType = "OldMap";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                newMapName.setEnabled(false);//Text box disabled
                startButton.setEnabled(false);//Start button disabled
            }
        });//Spinner listener

        //Text Box Listener
        newMapName.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                startButton.setEnabled(false);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                startButton.setEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s != null) {
                    startButton.setEnabled(true);
                    lawnSelected = s.toString();//Name of Lawn inputted by user
                }
            }
        });//Text box listener

/////////////////////////////BUTTON PRESS LISTENERS/////////////////////////////////////////////////
        //Connect Button
        connectButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                try {
                    ConnectBT.getInstance().setupBluetoothConnection();//Start BT Connection
                    ConnectBT.getInstance().write("Maps", "0");//Ask for maps in memory
                    String allMaps = ConnectBT.getInstance().read();//Listen for maps in memory
                    listMaps(getArgument(allMaps));
                }catch(Exception e){
                    Toast.makeText(getApplicationContext(),"Connection Error",Toast.LENGTH_SHORT).show();
                }
            }
        });//Connect Button

        //Go Home Button
        goHomeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                try {
                    ConnectBT.getInstance().write("Go Home", "0");
                }catch(Exception e) {
                    Toast.makeText(getApplicationContext(),"Go Home Command Error",Toast.LENGTH_SHORT).show();
                }

            }
        });//Go Home Button

        //Stop Button
        stopButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                try{
                    ConnectBT.getInstance().write("Stop","0");
                }catch(Exception e) {
                    Toast.makeText(getApplicationContext(),"Stop Command Error",Toast.LENGTH_SHORT).show();
                }
            }
        });//Stop Button

        //Update Button
        updateButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                try {
                    messageReceived = ConnectBT.getInstance().read();
                    String[] commands = messageReceived.split("\n");
                    for (int i = 0; i < commands.length; i++) {
                        messageCommand = getCommand(commands[i]);
                        messageArgument = getArgument(commands[i]);
                        Toast.makeText(getApplicationContext(),messageArgument,Toast.LENGTH_SHORT).show();
                        selectAction(messageCommand, messageArgument);
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Update Command Error", Toast.LENGTH_SHORT).show();
                }
            }
        });//Update Button

        //Start Button
        startButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                ConnectBT.getInstance().write("Start", "0");//Send Start Command

                try {
                    Thread.sleep(500);//Wait 0.5s
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ConnectBT.getInstance().write(lawnType, lawnSelected);//Send new or old lawn and name
            }
        });//Start Button

    }//onCreate

    /////////////////////////////////////////////////FUNCTIONS//////////////////////////////////////
    //clears obstacle and position points on map
    public void clearMap(){
        view = mapImageView;
        vWidth = view.getWidth();
        vHeight = view.getHeight();
        mBitmap = Bitmap.createBitmap(vWidth, vHeight, Bitmap.Config.ARGB_4444);
        mapImageView.setImageBitmap(mBitmap);
        mCanvas = new Canvas(mBitmap);
        mCanvas.drawColor(clearColor);
    }

    //For drawing obstacles one at a time
    public void drawObstacle(String input) {
        String coordinates[] = input.split(",");//split into x and y coordinates
        X.add(Float.valueOf(coordinates[0]));//add x coordinate to saved list
        Y.add(Float.valueOf(coordinates[1]));//add y coordinate to saved list
        float minX = Collections.min(X);//find smallest value of x
        float minY = Collections.min(Y);//find smallest value of y
        List<Float> newX = new ArrayList<Float>();//temporary x coordinates to be shifted and scaled
        List<Float> newY = new ArrayList<Float>();//temporary y coordinates to be shifted and scaled
        //Defining map area
        view = mapImageView;
        vWidth = view.getWidth();
        vHeight = view.getHeight();
        mBitmap = Bitmap.createBitmap(vWidth, vHeight, Bitmap.Config.ARGB_4444);
        mapImageView.setImageBitmap(mBitmap);
        mCanvas = new Canvas(mBitmap);

        for (int i = 0; i <Y.size(); i++) {
            if (minX<0) {//if x contains a negative value
                xShift = Math.abs(minX);//create shift variable equal to magnitude of minX
                newX.add(X.get(i) + Math.abs(minX));//add the minX to all values. Left with only positive in newX
            }
            else{newX = X;}//if no negative values, the two arrays are equal

            if (minY<0) {//if Y contains a negative value
                yShift = Math.abs(minY);//create shift variable equal to magnitude of minY
                newY.add(Y.get(i) + Math.abs(minY));//add the minY to all values. Left with only positive in newY
            }
            else{newY=Y;}//if no negative values, the two arrays are equal
        }//for loop

        int xRange = (int) (Collections.max(newX) - Collections.min(newX));//find range of x points
        int yRange = (int) (Collections.max(newY) - Collections.min(newY));//find range of y points
        if (xRange == 0){//range cannot be 0 - force to be 1
            xRange = 1;
        }
        if (yRange == 0){//range cannot be 0 - force to be 1
            yRange = 1;
        }
        int xScale = (vWidth/xRange);//[pixels/m] - x scaling factor
        int yScale = (vHeight/yRange);//[pixels/m] - y scaling factor
        if (xScale<yScale){//choose the smaller value to be scaling factor for x and y so map is not distorted
            scaleFactor = (int)(xScale*0.9);//take 90% to fit better in imageView
        }
        else {
            scaleFactor = (int)(yScale*0.9);
        }

        xSpacing = (int)((vWidth-xRange*scaleFactor))/2;//excess x space to center image
        ySpacing = (int)(((vHeight-yRange*scaleFactor))/2);//excess y space to center image
        for (int i = 0; i <X.size(); i++) {//loop through newX and newY and draw border
            mCanvas.drawCircle(newX.get(i) * scaleFactor+xSpacing, newY.get(i) * -scaleFactor+vHeight-ySpacing, 30, borderPaint);
        }
    }//drawObstacle

    //Draws full border when given all points
    public void drawMap(String input) {
        String[] inputs = input.split(";|:");
        XMap = getXCoordinates(inputs);
        YMap = getYCoordinates(inputs);
        float minX = Collections.min(X);
        float minY = Collections.min(Y);
        List<Float> newX = new ArrayList<Float>();
        List<Float> newY = new ArrayList<Float>();
        //Define map area
        view = mapImageView;
        vWidth = view.getWidth();
        vHeight = view.getHeight();
        mBitmap = Bitmap.createBitmap(vWidth, vHeight, Bitmap.Config.ARGB_4444);
        mapImageView.setImageBitmap(mBitmap);
        mCanvas = new Canvas(mBitmap);
        //Shift so all values are positive
        for (int i = 0; i <X.size(); i++) {
            if (minX<0) {
                newX.add(X.get(i) + Math.abs(minX));
                xShift = Math.abs(minX);
            }
            if (minY<0) {
                newY.add(Y.get(i) + Math.abs(minY));
                yShift = Math.abs(minY);
            }
            else{
                newX = X;
                newY = Y;
            }
        }//for loop

        //Find x and y ranges
        int xRange = (int) (Collections.max(X) - Collections.min(X));
        int yRange = (int) (Collections.max(Y) - Collections.min(Y));
        if (xRange == 0){//range cannot be 0 - force to be 1
            xRange = 1;
        }
        if (yRange == 0){//range cannot be 0 - force to be 1
            yRange = 1;
        }
        //Determine scale factor
        int xScale = (vWidth/xRange);
        int yScale = (vHeight/yRange);
        if (xScale<yScale){
            scaleFactor = (int)(xScale*0.9);
        }
        else {
            scaleFactor = (int)(yScale*0.9);
        }

        xSpacing = (int)((vWidth-xRange*scaleFactor))/2;//centering x
        ySpacing = (int)(((vHeight-yRange*scaleFactor))/2);//centering y
        for (int i = 0; i <X.size(); i++) {//draw border
            mCanvas.drawCircle(newX.get(i) * scaleFactor + xSpacing, newY.get(i) * -scaleFactor+vHeight-ySpacing, 30, borderPaint);
        }
    }

    //Draws robot's location
    public void drawPos(String input){
        String coordinates[] = input.split(",");
        float xpos = Float.valueOf(coordinates[0]);
        float ypos = Float.valueOf(coordinates[1]);
        mapImageView.setImageBitmap(mBitmap);
        mCanvas.drawCircle((xpos+xShift) * scaleFactor + xSpacing, (ypos+yShift) * -scaleFactor+vHeight-ySpacing, 30, posPaint);
    }

    //converts string of map names to String[] of map nampes
    public String[] listMaps(String input){
        String[] maps = input.split(",");
        for(int i=0; i< maps.length;i++){
            lawnChoices.add(maps[i]);
        }
        return maps;
    }

    //Removes just x coordinates from list
    public List<Float> getXCoordinates(String[] nums){
        List<Float> xCoordinates = new ArrayList<Float>();
        for (int i = 0; i <nums.length; i++) {
            if (i%2 ==0){
                xCoordinates.add(Float.parseFloat(nums[i]));
            }
        }
        return xCoordinates;
    }

    //Removes just y coordinates from list
    public List<Float> getYCoordinates(String[] nums){
        List<Float> yCoordinates = new ArrayList<Float>();
        for (int i = 0; i <nums.length; i++) {
            if (i%2 !=0){
                yCoordinates.add(Float.parseFloat(nums[i]));
            }
        }
        return yCoordinates;
    }

    //Separates first part of received message from arguments
    public String getCommand(String message){
        String[] parts = message.split(":");
        return parts[0];
    }

    //Separates second part of received message from command
    public String getArgument(String message){
        String[] parts = message.split(":");
        return parts[1];
    }

    //Calls proper function based on given command
    public void selectAction (String command, String argument){
        Toast.makeText(getApplicationContext(),command,Toast.LENGTH_SHORT).show();
        if (command.equals("Battery")){
            Toast.makeText(getApplicationContext(),command,Toast.LENGTH_SHORT).show();
            updateBattery(argument);
        }
        else if (command.equals("Obstacle")){
            drawObstacle(argument);
        }
        else if (command.equals("Position")){
            drawPos(argument);
        }
        else if (command.equals("Map")){
            drawMap(argument);
        }
    }

    //Updates battery life onscreen
    public void updateBattery(String batteryLife){
        batteryLifeText.setText(batteryLife + "%");
    }

    //Thread for reading BT input
    class BTListener implements Runnable{
        @Override
        public void run(){

            handler = new Handler(Looper.getMainLooper());
            runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        messageReceived = ConnectBT.getInstance().read();
                        String[] commands = messageReceived.split("\n");
                        for (int i = 0; i < commands.length; i++) {
                            messageCommand = getCommand(commands[i]);
                            messageArgument = getArgument(commands[i]);
                            //Toast.makeText(getApplicationContext(),messageArgument,Toast.LENGTH_SHORT).show();
                            selectAction(messageCommand, messageArgument);
                        }//for
                    } catch (Exception e) {
                        //Toast.makeText(getApplicationContext(), "No Data", Toast.LENGTH_SHORT).show();
                    }//catch
                    handler.postDelayed(this, 1000);
                }
            };
            handler.postDelayed(runnable, 1000);
        }//run
    }//BTListener


}//MainActivity
