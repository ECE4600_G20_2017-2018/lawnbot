package com.lawnbotgui2.lawnbotgui2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;

public class Main2Activity extends AppCompatActivity {

    public Button twoToOneButton;
    public Button resolutionButton;
    public Button clustersButton;
    public Button toleranceButton;
    public Button contrastButton;
    public Button obstacleButton;
    public Button bufferButton;
    public ConnectThread CThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        twoToOneButton = (Button) findViewById(R.id.twotoone);
        resolutionButton = (Button) findViewById(R.id.resolutionButton);
        clustersButton = (Button) findViewById(R.id.clustersButton);
        toleranceButton = (Button) findViewById(R.id.toleranceButton);
        contrastButton = (Button) findViewById(R.id.contrastButton);
        obstacleButton = (Button) findViewById(R.id.obstacleButton);
        bufferButton = (Button) findViewById(R.id.bufferButton);



        //Two to One Button
        twoToOneButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });//Two to One Button
        //Resolution Button
        resolutionButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

            }
        });//Resolution Button
        //Clusters Button
        clustersButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

            }
        });//Clusters Button
        //Tolerance Button
        toleranceButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

            }
        });//Tolerance Button
        //Contrast Button
        contrastButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

            }
        });//Contrast Button
        //Obstacle Button
        obstacleButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

            }
        });//Obstacle Button
        //Buffer Button
        bufferButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

            }
        });//Buffer Button

    }
}
