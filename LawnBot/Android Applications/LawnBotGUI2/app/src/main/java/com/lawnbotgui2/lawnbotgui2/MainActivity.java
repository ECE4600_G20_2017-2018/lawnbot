package com.lawnbotgui2.lawnbotgui2;

//////////////////////////IMPORTS///////////////////////////////////////////////////////////////////
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import java.util.Set;
import java.util.UUID;


public class MainActivity extends Activity {
    private TextView out;
    private static final int REQUEST_ENABLE_BT = 1;
    public Button stopButton;
    public Button goHomeButton;
    public Button forwardButton;
    public Button rightButton;
    public Button leftButton;
    public Button backButton;
    public Button oneToTwoButton;
    public ConnectThread CThread;
    public UUID MY_UUID = UUID.fromString("936DA01F-9ABD-4D9D-80C7-02AF85C822A8");
    public String MACAddress = "B8:27:EB:E1:8F:3F";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Define Layout
        setContentView(R.layout.activity_main);
        //Define Text Box
        out = (TextView) findViewById(R.id.Data);
        //Define Buttons
        stopButton = (Button) findViewById(R.id.stop);
        goHomeButton = (Button) findViewById(R.id.goHome);
        forwardButton = (Button) findViewById(R.id.forward);
        rightButton = (Button) findViewById(R.id.right);
        leftButton = (Button) findViewById(R.id.left);
        backButton = (Button) findViewById(R.id.backward);
        oneToTwoButton = (Button) findViewById(R.id.onetotwo);

        //Ensure Bluetooth is turned on
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //Check if Bluetooth is supported
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            out.setText("Bluetooth Not Supported");
        }
        //Request to enable Bluetooth
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }


        //////////////////////////////PAIRING///////////////////////////////////////////////////////
        BluetoothDevice mDevice;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.isEmpty()) {
            out.append("No devices paired");
            return ;
        }

        for (BluetoothDevice device : pairedDevices) {
            if (MACAddress.equals(device.getAddress())) {
                out.append("Found Device \n");
                mDevice = device;
                out.append(device.getName());
                CThread = new ConnectThread(mDevice);
                out.append("\nPairing Successful");
                break;
            }
        }
/////////////////////////////BUTTON PRESS///////////////////////////////////////////////////////////

        //Go Home Button
        goHomeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                out.setText("Go Home");
                CThread.write("Go Home".getBytes());
            }
        });//Stop Button
        //Stop Button
        stopButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                out.setText("Stop");
                CThread.cancel();
            }
        });//Stop Button

        //Forward Button
        forwardButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                out.setText("Forward");
                CThread.write("Forward".getBytes());
            }
        });//Forward Button

        //Right Button
        rightButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                out.setText("Right");
                CThread.write("Right".getBytes());
            }
        });//Right Button

        //Left Button
        leftButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                out.setText("Left");
                CThread.write("Left".getBytes());
            }
        });//Left Button

        //Back Button
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                out.setText("Back");
                CThread.write("Back".getBytes());
            }
        });//Back Button

        //One to Two Button
        oneToTwoButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(intent);
            }
        });//Back Button

    }//onCreate


}//MainActivity
