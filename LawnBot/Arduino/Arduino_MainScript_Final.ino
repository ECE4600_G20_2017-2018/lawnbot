/**
    Main arduino script
    Authors: Hayden Banting, Dale Maxwell
    Version: 09 March 2018
**/

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

//***********************************************************************************//
// Defines and Pinouts
//***********************************************************************************//

#define CLK      10  

#define TQ_1     29  
#define AUTO_1   25  
#define Vref_1   A5  
#define M1_1     31  
#define M2_1     23  
#define M3_1     35  
#define ENABLE_1 41  
#define RESET_1  37  
#define CW_CCW_1 43  
#define MO_1     39  

#define TQ_2     45  
#define AUTO_2   53 
#define Vref_2   A7  
#define M1_2     47 
#define M2_2     51  
#define M3_2     49  
#define ENABLE_2 48  
#define RESET_2  52  
#define CW_CCW_2 46  
#define MO_2     50  

#define TLV_NEG  A2 
#define TLV_POS  A6 

#define GYRO     40  

#define ECHO1    26
#define ECHO2    28
#define ECHO3    30
#define ECHO4    32
#define ECHO5    34
#define SONAR_VCC     36
#define TRIG1     A4
#define TRIG2     A3
#define TRIG3     A1
#define TRIG4     A0
#define TRIG5     A8

//***********************************************************************************//
// Comms Constants and Parmaters
//***********************************************************************************//
Adafruit_BNO055 bno = Adafruit_BNO055();
double pos = 0, neg = 0, curr = 0, coulomb = 0;
unsigned long timeStamp;
String hello = "[Mega] Hello from Arduino\n";
String msg_rec = "[Mega] Ready to start\n";
String waiting = "[Mega] Waiting for Raspberry Pi to send a signal...";
String inData;
char recieved;

//***********************************************************************************//
// Setup Function
//***********************************************************************************//
void setup() {
  Serial.begin(9600);
  Serial.println(waiting);
  
  pinMode(CLK, OUTPUT);

  pinMode(TQ_1, OUTPUT);
  pinMode(AUTO_1, OUTPUT);
  pinMode(M1_1, OUTPUT);
  pinMode(M2_1, OUTPUT);
  pinMode(M3_1, OUTPUT);
  pinMode(ENABLE_1, OUTPUT);
  pinMode(RESET_1, OUTPUT);
  pinMode(CW_CCW_1, OUTPUT);
  pinMode(MO_1, INPUT);

  pinMode(TQ_2, OUTPUT);
  pinMode(AUTO_2, OUTPUT);
  pinMode(M1_2, OUTPUT);
  pinMode(M2_2, OUTPUT);
  pinMode(M3_2, OUTPUT);
  pinMode(ENABLE_2, OUTPUT);
  pinMode(RESET_2, OUTPUT);
  pinMode(CW_CCW_2, OUTPUT);
  pinMode(MO_2, INPUT);

  pinMode(GYRO, OUTPUT);

  pinMode(ECHO1, INPUT);
  pinMode(ECHO2, INPUT);
  pinMode(ECHO3, INPUT);
  pinMode(ECHO4, INPUT);
  pinMode(ECHO5, INPUT);
  pinMode(SONAR_VCC, OUTPUT);

  digitalWrite(TQ_1, HIGH);    
  digitalWrite(AUTO_1, HIGH);   
  digitalWrite(M1_1, HIGH);   
  digitalWrite(M2_1, LOW);
  digitalWrite(M3_1, HIGH);
  digitalWrite(ENABLE_1, LOW); 
  digitalWrite(RESET_1, LOW);   
  digitalWrite(CW_CCW_1, HIGH);  
  analogWrite(Vref_1, 255);

  digitalWrite(TQ_2, HIGH);     
  digitalWrite(AUTO_2, HIGH);   
  digitalWrite(M1_2, HIGH);
  digitalWrite(M2_2, LOW);
  digitalWrite(M3_2, HIGH);
  digitalWrite(ENABLE_2, LOW);  
  digitalWrite(RESET_2, LOW);   
  digitalWrite(CW_CCW_2, LOW);  
  analogWrite(Vref_2, 255);

  digitalWrite(GYRO, LOW);

  digitalWrite(SONAR_VCC, LOW);

  TCCR2A = _BV(COM2A0) | _BV(COM2B1) | _BV(WGM20);
  TCCR2B = _BV(WGM02) | _BV(CS22) | _BV(CS21);
  OCR2A = 127;
  OCR2B = 64;
}

//***********************************************************************************//
// Main loop
//***********************************************************************************//
void loop() {

  timeStamp = micros();
  
  while (Serial.available() > 0) {

    recieved = Serial.read(); 
    inData += recieved; 

    if (recieved == '\n') { 

      if (inData.indexOf("FORWARD") > 0) {
        float distance = fetchCommandArg(inData);
        coloumbCounter(timeStamp);
        moveForward(distance);
        timeStamp = micros();
        writeData("[Mega] Pi asked to move forward: " + String(distance) + "m. Command complete.\n");
      }
      else if (inData.indexOf("ROTATE") > 0) {
        float angle = fetchCommandArg(inData);
        coloumbCounter(timeStamp);
        rotate(angle);
        timeStamp = micros();
        writeData("[Mega] Pi asked to rotate : " + String(angle) + "deg. Command complete.\n");
      }
      else if (inData.indexOf("COLLECT") > 0) {
        coloumbCounter(timeStamp);
        sonarArray();
        timeStamp = micros();
      }
      else if (inData.indexOf("BATTERY") > 0) {
        coloumbCounter(timeStamp);
        writeData(String(coulomb) + "\n");
        timeStamp = micros();
      }
      else {
        writeData("[Mega] Arduino received: " + inData + "\n");
      }
      inData = ""; // Clear recieved buffer
    }
  }
}

//***********************************************************************************//
// Function rotate
//***********************************************************************************//

void rotate(float degree) {

  float angle = 0.0;

  digitalWrite(GYRO, HIGH);
  delay(10);
  timeStamp = micros();
  
  if (!bno.begin()) {
    while (1);
  }
  bno.setExtCrystalUse(true);

  if (degree < 0) { 
    digitalWrite(CW_CCW_1, LOW); 
    digitalWrite(CW_CCW_2, LOW);   
    digitalWrite(ENABLE_1, HIGH);
    digitalWrite(ENABLE_2, HIGH);
    digitalWrite(RESET_1, HIGH);
    digitalWrite(RESET_2, HIGH);

    while (abs(angle + degree) > 0.05) {
      if (digitalRead(CLK) == HIGH) { 
        coloumbCounter(timeStamp);
        timeStamp = micros();
        
        while (digitalRead(CLK) == HIGH) {}
      }
      imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
      angle = euler.x();
    }
  }
  else {
    digitalWrite(CW_CCW_1, HIGH);  
    digitalWrite(CW_CCW_2, HIGH); 
    digitalWrite(ENABLE_1, HIGH);
    digitalWrite(ENABLE_2, HIGH);
    digitalWrite(RESET_1, HIGH);
    digitalWrite(RESET_2, HIGH);

    while (abs((360 - angle - degree)) > 0.05) {
      if (digitalRead(CLK) == HIGH) { 
        coloumbCounter(timeStamp);
        timeStamp = micros();
        
        while (digitalRead(CLK) == HIGH) {}
      }
      imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
      angle = euler.x();
    }
  }
  coloumbCounter(timeStamp);
  digitalWrite(ENABLE_1, LOW);
  digitalWrite(ENABLE_2, LOW);
  digitalWrite(RESET_1, LOW);
  digitalWrite(RESET_2, LOW);
  digitalWrite(GYRO, LOW);
}

//***********************************************************************************//
// Function moveForward
//***********************************************************************************//

void moveForward(float distance) {
  float circumference = ((0.1 * 2.0 * 3.14159) * 1000.0); // millimeters
  float ratio = ((distance * 1000.0) / circumference) * 505*8.0;
  int steps = round(ratio); // 
  int stepCount = 0;
  timeStamp = micros();
  
  digitalWrite(CW_CCW_1, HIGH);  
  digitalWrite(CW_CCW_2, LOW);   
  digitalWrite(ENABLE_1, HIGH);
  digitalWrite(ENABLE_2, HIGH);
  delay(20);
  digitalWrite(RESET_1, HIGH);
  digitalWrite(RESET_2, HIGH);

  while (stepCount < steps) {
    if (digitalRead(CLK) == HIGH) { //This was changed from HIGH
      coloumbCounter(timeStamp);
      timeStamp = micros();
      while (digitalRead(CLK) == HIGH) {} //This was changed from HIGH
      stepCount++;
    }
  }
  coloumbCounter(timeStamp);
  digitalWrite(ENABLE_2, LOW);
  digitalWrite(ENABLE_1, LOW);
  digitalWrite(RESET_1, LOW);
  digitalWrite(RESET_2, LOW);
}

//***********************************************************************************//
// Function coloumbCount
//***********************************************************************************//

void coloumbCounter(unsigned long timer) {
  pos = analogRead(TLV_POS);
  neg = analogRead(TLV_NEG);
  curr = (pos - neg) / 1023 * 5 / 0.1175;
  coulomb += curr*((micros()-timer)/1000000.0);
}

//***********************************************************************************//
/** Function SonarArray
    This function makes multiple calls to SingleSonar to collect data from every
    sonar sensor. It also used writeData to send the sonar data after every
    measurement is made.

*/
//***********************************************************************************//

void sonarArray() {

  float timeData;
  digitalWrite(SONAR_VCC, HIGH);
  timeStamp = micros();
  delay(1000);
  
  timeData = singleSonar(ECHO1, TRIG1);
  writeData(String(timeData) + ":");

  timeData = singleSonar(ECHO2, TRIG2);
  writeData(String(timeData) + ":");

  timeData = singleSonar(ECHO3, TRIG3);
  writeData(String(timeData) + ":");

  timeData = singleSonar(ECHO4, TRIG4);
  writeData(String(timeData) + ":");

  timeData = singleSonar(ECHO5, TRIG5);
  writeData(String(timeData) + "\n");

  coloumbCounter(timeStamp);
  digitalWrite(SONAR_VCC, LOW);
}
//***********************************************************************************//
/** Function singleSonar
    This function will read and return the time data for a sonar sensor. By
    specifying which sonar sensor trigger and echo pin to use you can collect data
    from a certain sensor.

    Param : TRIG - Sensor trigger pin
    Param : ECHO - Sensor echo pin
    Return : pulseWidth - Time for sonar to respond to pulses (us)
*/
//***********************************************************************************//

float singleSonar(int ECHO, int TRIG) {
  long pulseWidth;

  analogWrite(TRIG, 255);
  delayMicroseconds(10);
  analogWrite(TRIG, 0);

  pulseWidth = pulseIn(ECHO, HIGH);

  delayMicroseconds(50000 - pulseWidth);
  return pulseWidth;
}

//***********************************************************************************//
/** Function fetchCommandArg
    This function will parse a recently read command and return the command argument
    in floatr format. For example if a "move forward" command is read, the distance to
    move forward will be returned. Another example is if "rotate" is read, the angle
    to rotate will be retuend.

    Params: String inData - the data read by Arduino

*/
//***********************************************************************************//

float fetchCommandArg(String inData) {
  inData.remove(0, inData.indexOf(" "));
  inData.remove(inData.indexOf("/n"));
  float arg = inData.toFloat();
  return arg;
}

//***********************************************************************************//
/** Function writeData
    This function writes a string to the serial bus. Its up to the user to include the
    end of transmission character if they want to terminate a line. Multiple calls to
    writeData will keep "stacking" data on the line if no end of transmission character
    is sent.

    Param : String msg - data to be written to serial output buffer
*/
//***********************************************************************************//

void writeData(String msg) {
  Serial.print(msg);
}
